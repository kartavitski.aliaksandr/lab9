import java.time.LocalTime;
import java.util.Arrays;
import java.util.Scanner;

public class Train {
    private String destination;
    private int trainNumber;
    private LocalTime time;

    public Train(String destination, int trainNumber, LocalTime time) {
        this.destination = destination;
        this.trainNumber = trainNumber;
        this.time = time;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public int getTrainNumber() {
        return trainNumber;
    }

    public void setTrainNumber(int trainNumber) {
        this.trainNumber = trainNumber;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "Train{" +
                "destination='" + destination + '\'' +
                ", trainNumber=" + trainNumber +
                ", time=" + time +
                '}';
    }

    public static Train[] getSortedTrains(Train[] trains) {
        boolean sorted = false;
        Train temp;
        while (!sorted) {
            sorted = true;
            for (int i = 0; i < trains.length - 1; i++) {
                if (trains[i].getTrainNumber() > trains[i + 1].getTrainNumber()) {
                    temp = trains[i];
                    trains[i] = trains[i + 1];
                    trains[i + 1] = temp;
                    sorted = false;
                }
            }
        }
        return trains;
    }

    public static void printTrainInfo(Train[] trains, int numTrain) {
        for (Train t : trains) {
            if (t.getTrainNumber() == numTrain) {
                System.out.println(t);
            }
        }
    }

    public static Train[] getSortedByDestinationAndTime(Train[] trains) {
        boolean sorted = false;
        Train temp;
        while (!sorted) {
            sorted = true;
            for (int i = 0; i < trains.length - 1; i++) {
                if (trains[i].getDestination().compareTo(trains[i + 1].getDestination()) > 0) {
                    temp = trains[i];
                    trains[i] = trains[i + 1];
                    trains[i + 1] = temp;
                    sorted = false;
                } else if (trains[i].getDestination().compareTo(trains[i + 1].getDestination()) == 0 &&
                        trains[i].getTime().isAfter(trains[i+1].getTime())) {
                    temp = trains[i];
                    trains[i] = trains[i + 1];
                    trains[i + 1] = temp;
                    sorted = false;

                }
            }
        }
        return trains;
    }

    public static void main(String[] args) {
        Train[] trains = new Train[5];
        trains[0] = new Train("Vegas", 24, LocalTime.of(22, 10));
        trains[1] = new Train("Vegas", 2, LocalTime.of(21, 10));
        trains[2] = new Train("Bruklin", 1, LocalTime.of(11, 10));
        trains[3] = new Train("Minsk", 51, LocalTime.of(2, 10));
        trains[4] = new Train("Vegas", 81, LocalTime.of(20, 10));
        // System.out.println(Arrays.toString(trains));
        // System.out.println(Arrays.toString(getSortedTrains(trains)));
        int num = new Scanner(System.in).nextInt();
        printTrainInfo(trains, num);
        getSortedByDestinationAndTime(trains);
        System.out.println(Arrays.toString(trains));
    }
}

