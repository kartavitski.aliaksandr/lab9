public class Student {
    private String fio;
    private int groupNum;
    private int[] performance = new int[5];

    public Student(String fio, int groupNum, int[] performance) {
        this.fio = fio;
        this.groupNum = groupNum;
        this.performance = performance;
    }

    public static void printExcelentStudents (Student[] students) {
        for (Student student: students) {
            boolean isExcelent = true;
            for (int mark: student.performance) {
                if (mark <9 )
                    isExcelent = false;

            }
            if (isExcelent){
                System.out.println(student.fio);
            }

        }

    }

    public static void main(String[] args) {
        Student[] students = new Student[10];
        students[0] = new Student("Ivanov A.B.", 1, new int[]{9, 10, 9, 7, 1});
        students[1] = new Student("Petrov N.B.", 2, new int[]{9, 10, 9, 10, 10});
        students[2] = new Student("Evronimus A.D.", 4, new int[]{9, 10, 9, 7, 1});
        students[3] = new Student("Ayanami R.B.", 2, new int[]{9, 10, 9, 7, 1});
        students[4] = new Student("Kartavitsky A.S.", 6, new int[]{9, 10, 9, 9, 10});
        students[5] = new Student("Raynolds A.K.", 10, new int[]{9, 10, 9, 9, 10});
        students[6] = new Student("Zverinas A.B.", 3, new int[]{9, 10, 9, 7, 1});
        students[7] = new Student("Smith S.T.", 8, new int[]{9, 10, 9, 7, 1});
        students[8] = new Student("Lavinge T.B.", 8, new int[]{9, 10, 9, 7, 1});
        students[9] = new Student("Megan F.B.", 8, new int[]{9, 10, 9, 7, 1});
        printExcelentStudents(students);
    }
}

